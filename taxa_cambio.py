from bs4 import BeautifulSoup
import lxml
import requests

#O id do DOLAR AMERICANO é 61
def obter_taxa_dia(data, moeda):
    data_inicio = data
    fim = data.split('/')
    fim[0] = str(int(fim[0])+1)
    data_fim = "/".join(fim)
    
    result = requests.post('https://ptax.bcb.gov.br/ptax_internet/consultaBoletim.do?method=consultarBoletim', data = {'RadOpcao':'1','DATAINI':data_inicio,'DATAFIM':data_fim,'ChkMoeda':moeda})

    soup = BeautifulSoup(result.text, 'lxml')
    resultado = {'compra':'','venda':''}
    for cell in soup.select('body > div > table > tbody > tr.fundoPadraoBClaro2 > td:nth-child(3)'):
        print(f'Valor de compra: {cell.text}')
        resultado['compra'] = cell.text
    for cell in soup.select('body > div > table > tbody > tr.fundoPadraoBClaro2 > td:nth-child(4)'):
        print(f'Valor de venda: {cell.text}')
        resultado['venda'] = cell.text
    return resultado

print(obter_taxa_dia('03/03/2020','61'))
