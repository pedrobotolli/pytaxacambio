from bs4 import BeautifulSoup
import pandas as pd
import os
import lxml
import requests

def obter_taxa_dia(data, gera_csv):
    data_inicio = data
    fim = data.split('/')
    fim[0] = str(int(fim[0])+1)
    data_fim = "/".join(fim)
    result = requests.post('https://ptax.bcb.gov.br/ptax_internet/consultaBoletim.do?method=consultarBoletim', data = {'RadOpcao':'2','DATAINI':data_inicio,'DATAFIM':data_fim,'ChkMoeda':'61'})
    soup = BeautifulSoup(result.text, 'lxml')
    moedas = map(lambda x: x.text, soup.select('div:nth-child(1) table.tabela:nth-child(7) tbody:nth-child(1) tr> td:nth-child(3)'))
    val_vendas = map(lambda x:x.text, soup.select('div:nth-child(1) table.tabela:nth-child(7) tbody:nth-child(1) tr> td:nth-child(4)'))
    val_compras = map(lambda x:x.text, soup.select('div:nth-child(1) table.tabela:nth-child(7) tbody:nth-child(1) tr> td:nth-child(5)'))
    tabela = pd.DataFrame(list(zip(moedas,val_vendas,val_compras)),columns=['moeda','compra','venda'])

    if gera_csv:
        tabela.to_csv(os.path.join(os.path.abspath('saida/'), f'{data_inicio.replace("/","-")}.csv'), index=False)
        
    return tabela

tabela = obter_taxa_dia('03/03/2020', False)

print(tabela[tabela['moeda']=='ZAR'])
